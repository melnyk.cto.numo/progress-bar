document.addEventListener('DOMContentLoaded', function () {
    const line = document.getElementById('progress-line');
    const percent = document.getElementById('progress-percent');
    let count = 0;
    const progressBar = () => {
        count++;
        line.style.left = `${count}%`;
        percent.innerHTML = `${count}%`;
        percent.style.left = `${count}%`;
        if (count === 100) clearInterval(setIntervalId)
    };
    const setIntervalId = setInterval(progressBar, 50);
});


/*
 0. Стилізувати прогрес бар;
 1. Добавити кнопку "Start" (запуск прогрес бара);
 2. Добавити кнопку "Pause" (зупинка прогрес бара в точці);
 3. Добавити кнопку "Back" (прогрес бар заповнюється назад);
 */
